
var port = Number(process.env.PORT || 8000);

var express = require('express'); 
var app = express();
var	bodyParser = require('body-parser');

app.use(bodyParser.json());

var mongoose  = require('mongoose');

// Conexión con la base de datos
mongoose.connect('mongodb://localhost:27017/proyecto-inmobi');


var Portf = mongoose.model('Portf', {  
  id: Number,
  title: String, 
  status: String, 
  price: Number, 
  city: String, 
  bedrooms: Number, 
  bathrooms: Number, 
  imgUrl: String, 
  building_size: Number, 
  plot_size: Number, 
  features: String
});

// Rutas de nuestro API
app.get('/api/portfolios', function(req, res) {  
    Portf.find(function(err, datas) {
        if(err) {
            res.send(err);
        }
        res.json(datas);
    });
});

// POST que crea un TODO y devuelve todos tras la creación
app.post('/api/portfolios', function(req, res) {
    Portf.create({
        id: req.body.id,
        title: req.body.title,
        status: req.body.status,
        price: req.body.price,
        city: req.body.city,
        bedrooms: req.body.bedrooms,
        bathrooms: req.body.bathrooms,
        imgUrl: req.body.imgUrl,
        building_size: req.body.building_size,
        plot_size: req.body.plot_size,
        features: req.body.features
    }, function(err, todo){
        if(err) {
            res.send(err);
        }

        Portf.find(function(err, datas) {
            if(err){
                res.send(err);
            }
            res.json(datas);
        });
    });
});

// DELETE un TODO específico y devuelve todos tras borrarlo.
app.delete('/api/todos/:todo', function(req, res) {  
    Todo.remove({
        _id: req.params.todo
    }, function(err, todo) {
        if(err){
            res.send(err);
        }

        Todo.find(function(err, todos) {
            if(err){
                res.send(err);
            }
            res.json(todos);
        });

    })
});

app.use(express.static(__dirname + '/app'));
var server = app.listen(port, function() { console.log('Listening on port %d', server.address().port); });






//mongoose.connect('mongodb://nikko1801:nikkolo682@inmobiProy:27017);
//db.dropDatabase()

/*

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

// Connection URL
var url = 'mongodb://localhost:27017/proyecto-inmobi';
// Use connect method to connect to the Server
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server");
 	
	insertDocuments(db, function() {
  	db.close();
  });

});

var elements = [
	{"id": 1, "title": "title1", "status": "Sales", "price": 100000, "city": "Barcelona", "bedrooms": 4, "bathrooms":3, "imgUrl": "img/01.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 2, "title": "title2", "status": "Sales", "price": 220000, "city": "Girona", "bedrooms": 3, "bathrooms":3, "imgUrl": "img/02.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 3, "title": "title3", "status": "Sales", "price": 400000, "city": "Madrid", "bedrooms": 4, "bathrooms":3, "imgUrl": "img/03.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 4, "title": "title4", "status": "Sales", "price": 400000, "city": "Girona", "bedrooms": 2, "bathrooms":3, "imgUrl": "img/04.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 5, "title": "title5", "status": "Sales", "price": 500000, "city": "Costa del Sol", "bedrooms": 1, "bathrooms":3, "imgUrl": "img/05.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 6, "title": "title6", "status": "Sales", "price": 400000, "city": "Costa Brava", "bedrooms": 8, "bathrooms":3, "imgUrl": "img/06.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 7, "title": "title7", "status": "Sales", "price": 600000, "city": "Barcelona", "bedrooms": 9, "bathrooms":3, "imgUrl": "img/07.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 8, "title": "title8", "status": "Sales", "price": 800000, "city": "Barcelona", "bedrooms": 4, "bathrooms":3, "imgUrl": "img/08.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 9, "title": "title9", "status": "Sales", "price": 440000, "city": "Madrid", "bedrooms": 4, "bathrooms":3, "imgUrl": "img/09.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 10, "title": "title10", "status": "Sales", "price": 410000, "city": "Girona", "bedrooms": 1, "bathrooms":3, "imgUrl": "img/10.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 11, "title": "title11", "status": "Sales", "price": 100000, "city": "Barcelona", "bedrooms": 3, "bathrooms":3, "imgUrl": "img/11.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 12, "title": "title12", "status": "Sales", "price": 900000, "city": "Costa Blanca", "bedrooms": 4, "bathrooms":3, "imgUrl": "img/12.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 13, "title": "title13", "status": "Rental", "price": 140000, "city": "Costa Blanca", "bedrooms": 4, "bathrooms":3, "imgUrl": "img/01.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 14, "title": "title14", "status": "Rental", "price": 210000, "city": "Barcelona", "bedrooms": 3, "bathrooms":3, "imgUrl": "img/02.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 15, "title": "title15", "status": "Rental", "price": 450000, "city": "Madrid", "bedrooms": 4, "bathrooms":3, "imgUrl": "img/03.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 16, "title": "title16", "status": "Rental", "price": 405000, "city": "Madrid", "bedrooms": 2, "bathrooms":3, "imgUrl": "img/04.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 17, "title": "title17", "status": "Rental", "price": 504000, "city": "Girona", "bedrooms": 1, "bathrooms":3, "imgUrl": "img/05.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 18, "title": "title18", "status": "Rental", "price": 430000, "city": "Costa Brava", "bedrooms": 8, "bathrooms":3, "imgUrl": "img/06.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 19, "title": "title19", "status": "Rental", "price": 600000, "city": "Barcelona", "bedrooms": 9, "bathrooms":3, "imgUrl": "img/07.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 20, "title": "title20", "status": "Rental", "price": 830000, "city": "Madrid", "bedrooms": 4, "bathrooms":3, "imgUrl": "img/08.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 21, "title": "title21", "status": "Rental", "price": 1440000, "city": "Costa del Sol", "bedrooms": 4, "bathrooms":3, "imgUrl": "img/09.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 22, "title": "title22", "status": "Rental", "price": 410000, "city": "Girona", "bedrooms": 1, "bathrooms":3, "imgUrl": "img/10.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 23, "title": "title23", "status": "Rental", "price": 102000, "city": "Barcelona", "bedrooms": 3, "bathrooms":3, "imgUrl": "img/11.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"},
	{"id": 24, "title": "title24", "status": "Rental", "price": 900000, "city": "Costa del Sol", "bedrooms": 4, "bathrooms":3, "imgUrl": "img/12.jpg", "building_size": 300, "plot_size": 600, "features": "extra information"}
]

var insertDocuments = function(db, callback) {
  // Get the documents collection
  var collection = db.collection('portfolio');


  // Insert some documents
  collection.insertMany(elements, function(err, result) { //insert elements array in collection portfolio
    assert.equal(err, null);
    assert.equal(elements.length, result.result.n);
    assert.equal(elements.length, result.ops.length);
    //console.log("Inserted 3 documents into the document collection");
    callback(result);
  });
}
*/
'use strict';

angular.module('myApp.admin', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/admin', {
    templateUrl: 'admin/admin.html',
    controller: 'adminCtrl'
  });
}])

.controller('adminCtrl', ['$scope', '$http', function($scope, $http) {
  $http.get('mocks/content.json').then(function(response){
    $scope.data = response.data.data
  }, function (error) {
    //console.log('error', error)
    console.log('Ocurrió un error al consultar contenido.')
  })
}]);
'use strict';

angular.module('myApp.header', ['ngRoute'])

.controller('headerCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {

  $rootScope.changeLang = function (value) {
    if (value == 'ES') {
      $scope.isSpanish = true;
    }
    else {
      $scope.isSpanish = false;
    }
  }
}]);
'use strict';

angular.module('myApp.portfolio', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/portfolio', {
    templateUrl: 'portfolio/portfolio.html',
    controller: 'portfolioCtrl'
  });
}])

.controller('portfolioCtrl', ['$scope', '$http', '$rootScope', function($scope, $http, $rootScope) {
$rootScope
  $rootScope.changeLang('EN');
  
  $scope.mainFunction = function () {
    document.getElementById("filterBtn1").disabled = false;
    document.getElementById("filterBtn2").disabled = false;
    document.getElementById("filterBtn3").disabled = false;
    $http.get('mocks/content.json').then(function(response){
      $scope.data = response.data.data
      var objetos = $scope.data.map(function(data){
        return {"status": data.status, "city": data.city, "price": data.price};
      }); //Simplificar los JSONs con solo tres características
      dropdownFilters($scope.data) //call main function
    }, function (error) {
      //console.log('error', error)
      console.log('Ocurrió un error al consultar contenido.')
    })
  }

  $scope.mainFunction()

  var dropdownFilters = function (objetos) {
    $scope.filterStatus = [];
    $scope.filterCity = [];
    
    $scope.filterPrice = ["100.000 - 250.000", "250.000 - 500.000", "500.000 - 1.000.000", "1.000.000 - 3.000.000", "3.000.000 - 5.000.000", "5.000.000 - 10.000.000", "10.000.000 - 20.000.000", "+20.000.000"];

    $scope.priceForFilter = "Price";
    $scope.statusForFilter = "Status";
    $scope.cityForFilter = "City";
    $scope.showButtonStatus = false;
    $scope.showButtonCity = false;
    $scope.showButtonPrice = false;

    $scope.changeStatusForFilter = function (status) {
      $scope.statusForFilter = status.data
      $scope.showButtonStatus = true
    }

    $scope.changeCityForFilter = function (city) {
      $scope.cityForFilter = city.data
      $scope.showButtonCity = true
    }
   
    $scope.changePriceForFilter = function (price, ind) {
      $scope.priceForFilter = price
      $scope.showButtonPrice = true
      $scope.range = []

      if (ind) {
        if (ind == 1) {$scope.range = [250000, 500000]}
        else if (ind == 2) {$scope.range = [500000, 1000000]}
        else if (ind == 3) {$scope.range = [1000000, 3000000]}
        else if (ind == 4) {$scope.range = [3000000, 5000000]}
        else if (ind == 5) {$scope.range = [5000000, 10000000]}
        else if (ind == 6) {$scope.range = [10000000, 20000000]}
        else if (ind == 7) {$scope.range = [20000000, 100000000000]}
        else if (ind == 0) {$scope.range = [100000, 250000]}
      }
      else {$scope.range = [100000, 250000]}
    }

    $scope.applyFilter = function () {
      document.getElementById("filterBtn1").disabled = true;
      document.getElementById("filterBtn2").disabled = true;
      document.getElementById("filterBtn3").disabled = true;
      $scope.showButtonStatus = false
      $scope.showButtonCity = false
      $scope.showButtonPrice = false
      $scope.data_new = $scope.data
      $scope.data = []

      //console.log($scope.range[0], $scope.range[1])

      if ($scope.priceForFilter != "Price" && $scope.statusForFilter != "Status" && $scope.cityForFilter != "City") {
        $scope.data_new.forEach(function(dat){
          if (dat.price >= $scope.range[0] && dat.price <= $scope.range[1] && dat.status == $scope.statusForFilter && dat.city == $scope.cityForFilter) { 
            $scope.data.push(dat)
          }
        })
      }
      else if ($scope.priceForFilter != "Price" && $scope.statusForFilter == "Status" && $scope.cityForFilter == "City") {
        $scope.data_new.forEach(function(dat){
          if (dat.price >= $scope.range[0] && dat.price <= $scope.range[1]) { 
            $scope.data.push(dat)
          } 
        })  
      }
      else if ($scope.priceForFilter == "Price" && $scope.statusForFilter != "Status" && $scope.cityForFilter == "City") {
        $scope.data_new.forEach(function(dat){
          if (dat.status == $scope.statusForFilter) {
            $scope.data.push(dat)
          }
        })  
      }
      else if ($scope.priceForFilter == "Price" && $scope.statusForFilter == "Status" && $scope.cityForFilter != "City") {
        $scope.data_new.forEach(function(dat){
          if (dat.city == $scope.cityForFilter) {
            $scope.data.push(dat)
          }
        })  
      }
      else if ($scope.priceForFilter != "Price" && $scope.statusForFilter != "Status" && $scope.cityForFilter == "City") {
        $scope.data_new.forEach(function(dat){
          if (dat.status == $scope.statusForFilter && dat.price >= $scope.range[0] && dat.price <= $scope.range[1]) {
            $scope.data.push(dat)
          }
        })  
      }
      else if ($scope.priceForFilter != "Price" && $scope.statusForFilter == "Status" && $scope.cityForFilter != "City") {
        $scope.data_new.forEach(function(dat){
          if (dat.city == $scope.cityForFilter && dat.price >= $scope.range[0] && dat.price <= $scope.range[1]) {
            $scope.data.push(dat)
          }
        })  
      }
      else if ($scope.priceForFilter == "Price" && $scope.statusForFilter != "Status" && $scope.cityForFilter != "City") {
        $scope.data_new.forEach(function(dat){
          if (dat.city == $scope.cityForFilter && dat.status == $scope.statusForFilter) {
            $scope.data.push(dat)
          }
        })  
      }
      else {
        $scope.mainFunction()
      }
    }


    objetos.forEach(function(data){
      $scope.cont_city = 0;
      $scope.cont_status = 0;

      if ($scope.filterCity[0] == null) {
        $scope.filterCity.push({"data": data.city})
      }
      else if ($scope.filterStatus[0] == null) {
        $scope.filterStatus.push({"data": data.status})
      }
      else {

        $scope.filterCity.forEach(function(city){
          if (city.data == data.city) {
            $scope.cont_city = $scope.cont_city + 1
          }
        })
    
        if ($scope.cont_city == 0) {
          $scope.filterCity.push({"data": data.city})
        }

        $scope.filterStatus.forEach(function(status){
          if (status.data == data.status) {
            $scope.cont_status = $scope.cont_status + 1
          }
        })
    
        if ($scope.cont_status == 0) {
          $scope.filterStatus.push({"data": data.status})
        }
      }
    })
    //console.log($scope.filterCity, $scope.filterPrice, $scope.filterStatus)
  }

//150000 - 250000, 250000 - 500000, 500000 - 1000000, 1000000 - 3000000, 3000000 - 5000000, 5000000 - 10000000, 10000000 - 20000000, +2000000

}]);
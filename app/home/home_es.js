'use strict';

angular.module('myApp.homeEs', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/es', {
    templateUrl: 'home/home_es.html',
    controller: 'homeEsCtrl'
  });
}])

.controller('homeEsCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {
  $rootScope.changeLang('ES');
}]);
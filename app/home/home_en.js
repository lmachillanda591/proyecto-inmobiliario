'use strict';

angular.module('myApp.homeEn', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/en', {
    templateUrl: 'home/home_en.html',
    controller: 'homeEnCtrl'
  });
}])

.controller('homeEnCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {
  $rootScope.changeLang('EN');
}]);